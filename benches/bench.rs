use criterion::{black_box, criterion_group, criterion_main, Criterion};
use quicksort_ai::{quicksort_codellama, quicksort_opengenus};

use rand::distributions::{Distribution, Standard};

fn random_vec<T>(size: u32) -> Vec<T>
where
    Standard: Distribution<T>,
{
    (0..size).map(|_| rand::random::<T>()).collect()
}

fn bench(c: &mut Criterion) {
    for size in [10, 100, 1_000] {
        c.bench_function(&format!("codellama {}", size), |b| {
            let mut vec = random_vec::<i32>(black_box(size));
            b.iter(|| quicksort_codellama(&mut vec))
        });

        c.bench_function(&format!("opengenus {}", size), |b| {
            let mut vec = random_vec::<i32>(black_box(size));
            b.iter(|| quicksort_opengenus(&mut vec))
        });
    }
}

criterion_group!(benches, bench);
criterion_main!(benches);
