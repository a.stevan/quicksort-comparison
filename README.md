- `quicksort_codellama` has been generated with the CodeLlama model
- `quicksort_opengenus` comes from https://iq.opengenus.org/quick-sort-in-rust/

they both have tests that pass
```shell
cargo test
```

however, the Opengenus one is much faster
```
cargo criterion --output-format verbose --message-format json out> quicksort.ndjson
```
gives the following results
| experiment     | mean (ns) |
| -------------- | --------- |
| codellama 10   | 275       |
| opengenus 10   | 33        |
| codellama 100  | 13221     |
| opengenus 100  | 2506      |
| codellama 1000 | 834622    |
| opengenus 1000 | 196198    |
