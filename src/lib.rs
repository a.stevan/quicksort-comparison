use std::cmp::Ordering;

pub fn quicksort_codellama(arr: &mut Vec<i32>) {
    let len = arr.len();
    if len <= 1 {
        return;
    }

    let pivot = arr[0];
    let mut left = vec![];
    let mut right = vec![];

    for i in 1..len {
        match arr[i].cmp(&pivot) {
            Ordering::Less => left.push(arr[i]),
            Ordering::Greater => right.push(arr[i]),
            Ordering::Equal => continue,
        }
    }

    quicksort_codellama(&mut left);
    quicksort_codellama(&mut right);

    arr.clear();
    arr.extend_from_slice(&left);
    arr.push(pivot);
    arr.extend_from_slice(&right);
}

pub fn quicksort_opengenus(slice: &mut [i32]) {
    if !slice.is_empty() {
        let partition_index = partition(slice);
        let len = slice.len();

        quicksort_opengenus(&mut slice[0..partition_index]);
        quicksort_opengenus(&mut slice[partition_index + 1..len]);
    }
}

fn partition(slice: &mut [i32]) -> usize {
    let len = slice.len();
    let pivot = slice[len - 1];
    let mut i = 0;
    let mut j = 0;

    while j < len - 1 {
        if slice[j] <= pivot {
            slice.swap(i, j);
            i += 1;
        }
        j += 1;
    }

    slice.swap(i, len - 1);

    i
}

#[cfg(test)]
mod tests {
    use crate::{quicksort_codellama, quicksort_opengenus};

    fn is_sorted(slice: &[i32]) -> bool {
        for i in 1..slice.len() {
            if !(slice[i - 1] <= slice[i]) {
                return false;
            }
        }

        true
    }

    #[test]
    fn sorted() {
        assert!(is_sorted(&[0, 1, 2, 3]));
        assert!(is_sorted(&[0, 1, 2, 2, 2, 3]));
        assert!(!is_sorted(&[0, 2, 1, 3]));
    }

    fn random_vector(size: u32) -> Vec<i32> {
        let mut slice: Vec<i32> = Vec::new();
        for _ in 0..size {
            slice.push(rand::random::<i32>());
        }
        slice
    }

    #[test]
    fn opengenus() {
        for _ in 0..10 {
            let mut vec = random_vector(100);

            quicksort_opengenus(&mut vec);
            assert!(is_sorted(&vec));
        }
    }

    #[test]
    fn codellama() {
        for _ in 0..10 {
            let mut vec = random_vector(100);

            quicksort_codellama(&mut vec);
            assert!(is_sorted(&vec));
        }
    }
}
